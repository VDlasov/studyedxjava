import comp102x.IO;

public class Lab02
{
    
    public static void multiply()
    {
        // Please write your code after this line
        IO.output("Enter an integer, x: ");
        int numX = IO.inputInteger();
        IO.output("Enter an integer, y: ");
        int numY = IO.inputInteger();
        int numMult = numX * numY;
        IO.output("Answer = " + numMult);
    }
    
    public static void calculateTriangleArea()
    {
        // Please write your code after this line
        IO.output("Enter the width of the triangle: ");
        int width = IO.inputInteger();
        IO.output("Enter the height of the triangle: ");
        int height = IO.inputInteger();
        double triangleArea = width * height / 2.0;
        IO.output("The triangle area = " + triangleArea);
        
        
    }
    
    public static void solveQuadraticEquation()
    {
        // Please write your code after this line
        IO.output("Enter a: ");
        int a = IO.inputInteger();
        IO.output("Enter b: ");
        int b = IO.inputInteger();
        IO.output("Enter c: ");
        int c = IO.inputInteger();
        
        // Math.pow(b, 2)
        double first_x = (-b + Math.sqrt(b*b - 4 * a * c)) / (2.0 * a);
        double second_x = (-b - Math.sqrt(b*b - 4 * a * c)) / (2.0 * a);
        IO.outputln("First solution for x = " + first_x);
        IO.outputln("Second solution for x = " + second_x);
        
        
        
    }
}
