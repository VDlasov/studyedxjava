public class HelloWorld{


     public static void main(String []args){
        System.out.println("Hello World");
        
        System.out.println(countDivisible(3, 9));
        
            
        //System.out.println(( y && x ) && ( x || y ));
     }
     
     public static int countDivisible(int lowerBound, int upperBound) {
         int result = 0;
         
         for (int i = lowerBound; i <= upperBound; i++) {
             if (i % 3 == 0) {
                 if (i % 9 != 0) {
                     result++;
                 }
             }
         }
         return result;
     }
     
}
