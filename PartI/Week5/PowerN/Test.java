import comp102x.IO;
/**
 * Write a description of class Test here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Test
{
    // instance variables - replace the example below with your own
     private int a;

    /**
     * Constructor for objects of class Test
     */
    public Test()
    {
        // initialise instance variables
        a = 2;
    }

    public Test(int p) {
        a = p;
    } 
    
    /**
     * An example of a method - replace this comment with your own
     * 
     * @param  y   a sample parameter for a method
     * @return     the sum of x and y 
     */
        public void fun( ) {
            IO.outputln(a);
        }
        
        public void fun(int p, int q) {
            q = p / a;
            IO.outputln(a + p + q);
        }
        
        public void fun(int p, double q) {
            q = a / p;
            IO.outputln(a + p - q);
        }
        
        public static void main(String[ ] args) {
            Test c3 = new Test( 3 );
            c3.fun( (int)3.0, (int)2.0 );
            
        }
}
