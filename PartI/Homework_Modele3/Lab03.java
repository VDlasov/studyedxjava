import comp102x.ColorImage;
import comp102x.Canvas;

public class Lab03
{  
    
    public void loadAnImage() 
    {
        // Please write your code after this line
        ColorImage image1 = new ColorImage("A.jpg");
        Canvas canvas = new Canvas(image1.getWidth(), image1.getHeight());
        canvas.add(image1, 0, 0);
    }
    
    public void loadTwoImagesOnTheSameCanvas()
    {
        // Please write your code after this line
        ColorImage image1 = new ColorImage("A.jpg");
        ColorImage image2 = new ColorImage("B.jpg");
        Canvas canvas = new Canvas(image1.getWidth() + image2.getWidth(), image1.getHeight());
        canvas.add(image1, 0, 0);
        canvas.add(image2, image1.getWidth(), 0);
    }
    
    public void imageAddition() 
    {    
        // Please write your code after this line
        ColorImage image1 = new ColorImage("A.jpg");
        ColorImage image2 = new ColorImage("B.jpg");
        ColorImage splitBlack = image1.add(image2);
        Canvas canvas = new Canvas(3 * image1.getWidth() + 20, image1.getHeight());
        canvas.add(image1, 0, 0);
        canvas.add(image2, image1.getWidth() + 10, 0);
        canvas.add(splitBlack, 2 * image1.getWidth() + 10 + 10, 0);
    }
   
    public void imageMultiplication() 
    {
        // Please write your code after this line
        ColorImage image1 = new ColorImage("A.jpg");
        ColorImage image2 = new ColorImage("B.jpg");
        ColorImage splitWhite = image2.multiply(image1);
        Canvas canvas = new Canvas(3 * image1.getWidth() + 20, image1.getHeight());
        canvas.add(image1, 0, 0);
        canvas.add(image2, image1.getWidth() + 10, 0);
        canvas.add(splitWhite, 2 * image1.getWidth() + 10 + 10, 0);
    }
    
    public void changeColor()
    {
        ColorImage image = new ColorImage();
        image.increaseRed(40);
        Canvas canvas = new Canvas(image.getWidth(), image.getHeight());
        canvas.add(image);
        
        //image.save();
    }
}
