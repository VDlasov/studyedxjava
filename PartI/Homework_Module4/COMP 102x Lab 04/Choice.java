import comp102x.ColorImage;
import comp102x.Canvas; 

/**
 * The Choice class represents a choice made by the player or the computer.
 * It can be either "rock", "paper", or "scissors".
 */
public class Choice
{
    private int type; //stores the choice type: 0=rock, 1=paper, 2=scissors
    private ColorImage choiceImage; //stores the image to be displayed on the canvas
    
    /**
     * The constructor
     * 
     * @param   type   the choice type to be represented by this Choice object
     */
    public Choice(int type)
    {
        //initialize the "type" instance varialble
        this.type = type;
    }
    
    /**
     * Get a number that represents the choice type
     * 
     * @return  a number that represents the choice type: 0=rock, 1=paper, 2=scissors
     */
    public int getType()
    {
        return type;
    }
    
    /**
     * Compare "this" with anotherChoice
     * 
     * @param   anotherChoice   the choice to be compared
     * @return  either 1, -1, or 0 which indicates the comparison result: 1 means "this" wins anotherChoice; -1 means "this" loses to anotherChoice; 0 means "this" and anotherChoice are the same
     */
    public int compareWith(Choice anotherChoice)
    {
        // write your code after this line
        /*
        if (type == anotherChoice.getType())
        {
            return 0;
        }
        if (type == 0 && anotherChoice.getType()== 1)
        {
            return -1;
        }
        if (type == 0 && anotherChoice.getType()== 2)
        {
            return 1;
        }
        
        if (type == 1 && anotherChoice.getType()== 2)
        {
            return -1;
        }
        if (type == 1 && anotherChoice.getType()== 0)
        {
            return 1;
        }
        
        if (type == 2 && anotherChoice.getType()== 0)
        {
            return -1;
        }
        if (type == 2 && anotherChoice.getType()== 1)
        {
            return 1;
        }*/
        
        if (type > anotherChoice.getType())
        {
            if (type == 2 && anotherChoice.getType() == 0)
                return -1;
            return 1;
        }
        if (type < anotherChoice.getType())
        {
            if (type == 0 && anotherChoice.getType() == 2)
                return 1;
            return -1;        
        }
        
        return 0;
        //return 0; // this line should be modified/removed upon finishing the implementation of this method
    }
    
    /**
     * Draw the choice image (rock/paper/scissors) on the given canvas
     * 
     * @param   canvas      the canvas to draw on
     * @param   x           the x-position of the choice image to be drawn
     * @param   y           the y-position of the choice image to be drawn
     * @param   rotation    the rotation of the choice image to be drawn
     */ 
    public void draw(Canvas canvas, int x, int y, int rotation)
    {
       // write your code after this line
       
       if (type == 0)
       {
           ColorImage image1 = new ColorImage("rock.png");
           image1.setRotation(rotation);
           canvas.add(image1, x, y);
       }
       if (type == 1)
       {
           ColorImage image1 = new ColorImage("paper.png");
           image1.setRotation(rotation);
           canvas.add(image1, x, y);
       }
       if (type == 2)
       {
           ColorImage image1 = new ColorImage("scissors.png");
           image1.setRotation(rotation);
           canvas.add(image1, x, y);
       }
       
       
       
       
    }
}
