package com.company;

import java.util.Stack;

public class Main {

    public static void main(String[] args) {
        // write your code here
        task5();

    }

    public static void task5() {
        Stack<Integer> original = new Stack<Integer>();

        original.push(1);
        original.push(4);
        original.push(2);
        original.push(3);
        original.push(8);

        arrangeStack(original);

        System.out.println("END");
    }

    public static void arrangeStack(Stack<Integer> original) {
        Stack<Integer> even = new Stack<Integer>();
        Stack<Integer> odd = new Stack<Integer>();
        int temp = -1;

        while (!original.empty()) {
            temp = original.pop();
            if (temp % 2 == 0) {
                even.push(temp);
            }
            else {
                odd.push(temp);
            }
        }
        while (!odd.empty()) {
            original.push(odd.pop());
        }
        while (!even.empty()) {
            original.push(even.pop());
        }
    }

    public static void task4() {

        int result = reverseDigits(54321);
        System.out.println(result);
    }

    public static int reverseDigits(int n) {
        if (n < 10)
            return n;
        int result = (n%10) * powerOfTen(n) + reverseDigits(n/10);
        return result;
    }

    public static void task3() {
        int result = powerOfTen(5);
        System.out.println(result);
    }

    public static int powerOfTen(int n) {
        if (n < 10)
            return 1;
        else
            return powerOfTen(n/10) * 10;
    }


    public static void task2() {
        String result = removeRepeatedWords("cat fat cat cat hat");
        System.out.println(result);
    }

    public static String removeRepeatedWords(String inputString) {

        inputString = inputString.trim(); // Remove any leading and trailing blanks
        String result = "", currentWord = "", nextWord = "";
        int stringLength = inputString.length(); // Length of the input string
        int stringIndex = 0; // Index for input string

        while (stringIndex < stringLength) {

            // Get the next word in inputString

            while (  stringIndex < stringLength && inputString.charAt(stringIndex) != ' ') {

                nextWord = nextWord + inputString.charAt(stringIndex);
                stringIndex++;
            }

            // Check if nextWord is the same as currentWord

            if (  !nextWord.equals(currentWord)  ) {

                result = result + " " + nextWord;
            }

            stringIndex++;
            currentWord = nextWord;
            nextWord = "";
        }

        return result.trim(); // Remove any leading and trailing blanks from result

    }

    public static void task1() {
        int[][] a = {{0, 1, 2, 3, 2}, {1, 2, 3, 2, 1}, {2, 3, 2, 1, 0}};
        int high = 3;
        int[] result = histogram(a, high);
        for (int i = 0; i < result.length; i++) {
            System.out.println(result[i] + " ");
        }
    }

    public static int[] histogram(int[][] a, int high) {
        int[] result = new int[high+1];


        for (int k = 0; k < high+1; k++) {
            result[k] = 0;
            for (int i = 0; i < a.length; i++) {
                for (int j = 0; j < a[i].length; j++) {
                    if (a[i][j] == k)
                        result[k]++;
                }
            }
        }
        return result;
    }
}
