
public class Lab01Task1
{
    public int minPos(GameRecord[] records, int size) {
        int minPoz = 0;
        GameRecord minElem = records[0];
        for (int i = 0; i < size; i++) {
            if (records[i].getLevel() < minElem.getLevel() || (records[i].getLevel() == minElem.getLevel() && records[i].getScore() <= minElem.getScore())) {
                    minPoz = i;
                    minElem = records[minPoz];
            }
        }
        return minPoz; // this line should be removed or modified after the implementation of this method is completed.
    }
    
    public static void testCase1() {
    
        GameRecord[] records = new GameRecord[5];
        /*records[0] = new GameRecord("A", 2, 10);
        records[1] = new GameRecord("B", 3, 8);
        records[2] = new GameRecord("C", 2, 5);
        records[3] = new GameRecord("D", 1, 2);*/
        records[0] = new GameRecord("A", 2, 12);
        records[1] = new GameRecord("B", 0, 42);
        records[2] = new GameRecord("C", 0, 40);
        records[3] = new GameRecord("D", 0, 51);
        records[4] = new GameRecord("E", 1, 10);
        
        Lab01Task1 lab01Task1 = new Lab01Task1();
        int actualOutput = lab01Task1.minPos(records, 5); // only searching the first 3 elements
        
        System.out.println("Expected output: 2");
        System.out.println("Actual output: " + actualOutput);
    }
    
    // You can add more test cases to test your program prior to submitting your code to the online grader.
}
