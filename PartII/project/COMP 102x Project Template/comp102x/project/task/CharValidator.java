package comp102x.project.task;

public class CharValidator {
    
    public boolean validateChar(char c) {
        
        // Please write your code after  this line
        if ((c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z') || (c >= '0' && c <= '9')) {
            return true;
        }

        return false; // This line should be modified or removed after finising the implementation of this method.
    }

}
