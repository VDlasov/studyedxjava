package comp102x.project.task;

import java.util.Random;

import comp102x.project.model.Target;

public class TargetUpdater {
    
    public void updateTarget(Target[] targets, int level) {
        
        // Please write your code after this line
        
        int r1, r2;
        boolean hit1, hit2;
        Random r = new Random();
        int times = 0;
        
        switch (level) {
            case 1: times = 0;
            break;
            
            case 2: times = 4;
            break;
            
            case 3: times = 10;
            break;
        }
        
        for (int i = 0; i < times; i++) {
            r1 = r.nextInt(targets.length);
            r2 = r1;
            
            while(r2 == r1) {
                r2 = r.nextInt(targets.length);
            }
            
            hit1 = targets[r1].isHit();
            hit2 = targets[r2].isHit();
            
            if ( (hit1 == true && hit2 == false) || (hit1 == false && hit2 == true)) {
                //System.out.println("swap: " + r1 + ", " + r2);
                Target temp = targets[r1];
                targets[r1] = targets[r2];                
                targets[r2] = temp;
            }
        }
        
        
    }

}
